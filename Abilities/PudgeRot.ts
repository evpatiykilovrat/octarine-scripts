import { ActiveAbility, IToggleable } from "immortal-core/Imports";
import { AIOUsable, TargetManager } from "_ICore_/Index";

export class PudgeRot extends AIOUsable {

	private readonly rot: ActiveAbility & IToggleable

	constructor(ability: ActiveAbility) {
		super(ability)
		this.rot = ability as ActiveAbility & IToggleable
	}

	public get IsEnabled(): boolean {
		return this.rot.Enabled
	}

	public ForceUseAbility(): boolean {
		return false
	}

	public AutoToggle(): boolean {
		if (this.rot.Enabled) {
			if (!this.ShouldCast())
				return this.UseAbility(false)
		} else {
			if (this.ShouldCast())
				return this.UseAbility(false)
		}
		return false
	}

	public ShouldCast(): boolean {
		const target = TargetManager.Target!
		if (TargetManager.TargetLocked && !target.IsVisible)
			return true

		if (!this.rot.CanHit(target) || this.Owner.BaseOwner.IsInAbilityPhase)
			return false

		if (target.IsInvulnerable)
			return false

		return true
	}

	public UseAbility(aoe: boolean, ...args: any[]): boolean {
		this.rot.Enabled = !this.rot.Enabled
		this.rot.ActionSleeper.Sleep(0.1)
		return true
	}
}
