import { ActiveAbility, Dismember, UnitX } from "immortal-core/Imports";
import { AIOBlink, AIOUsable } from "_ICore_/Index";

export class BlinkDaggerPudge extends AIOBlink {
	constructor(ability: ActiveAbility) {
		super(ability)
	}

	public ShouldConditionCast(target: UnitX, usableAbilities: Nullable<AIOUsable>[]) {
		const dismember = usableAbilities.find(x => x !== undefined && x.Ability instanceof Dismember)
		return dismember !== undefined
	}

	public UseAbility(aoe: boolean) {
		return super.UseAbility(aoe, 100, 25)
	}
}
