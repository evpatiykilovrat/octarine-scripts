import { ActiveAbility, HitChanceX, OrbWalker, UnitX } from "immortal-core/Imports";
import { GameState } from "wrapper/Imports";
import { AIONuke, HERO_DATA, TargetManager } from "_ICore_/Index";
import { ComboHookDelay, PudgeMenu } from "../menu";

export class PudgeHook extends AIONuke {

	private rotation: number = 0
	private rotationTime: number = 0
	private lastTarget: Nullable<UnitX>

	constructor(ability: ActiveAbility) {
		super(ability)
		this.breakShields.concat(["modifier_templar_assassin_refraction_absorb"])
	}

	public get GetPrediction() {
		return this.Ability.GetPredictionOutput(this.Ability.GetPredictionInput(TargetManager.Target!))
	}

	public CanHit() {

		const target = TargetManager.Target!
		const predictionInput = this.Ability.GetPredictionInput(target)
		const output = this.Ability.GetPredictionOutput(predictionInput)

		if (output.HitChance <= HitChanceX.Impossible)
			return false

		if (target.Distance(this.Owner.BaseOwner) < 300 || target.GetImmobilityDuration > 0)
			return true

		if (target.Equals(this.lastTarget)) {
			if (Math.abs(this.rotation - target.BaseOwner.RotationRad) > 0.1) {
				this.rotationTime = GameState.RawGameTime
				this.rotation = target.BaseOwner.RotationRad
				return false
			}

			let delay = 0
			if (PudgeMenu.ComboKey.is_pressed) {
				delay = ComboHookDelay.value
			} else if (PudgeMenu.HarassKey.is_pressed)
				delay = ComboHookDelay.value

			if (this.rotationTime + (delay / 1000) > GameState.RawGameTime)
				return false

		} else {
			this.lastTarget = target
			this.rotationTime = GameState.RawGameTime
			this.rotation = target.BaseOwner.RotationRad
			return false
		}
		return true
	}

	public ShouldCast() {
		const target = TargetManager.Target!
		if (target.HasBuffByName("modifier_templar_assassin_refraction_absorb") && target.Distance(this.Owner) < 300)
			return false
		return super.ShouldCast()
	}

	public UseAbility(aoe: boolean) {
		if (!super.UseAbility(aoe))
			return false
		if (this.lastTarget !== undefined) {
			this.lastTarget.RefreshUnitState()
			this.lastTarget = undefined
		}
		const delay = this.Ability.GetHitTime(TargetManager.Target!)
		const orbWSleep = delay + (this.Owner.Distance(TargetManager.Target!) / this.Ability.Speed)
		OrbWalker.Sleeper.Sleep(orbWSleep, this.Owner.Handle)
		HERO_DATA.ComboSleeper.Sleep(delay + 0.1, this.Owner.Handle)
		return true
	}
}
