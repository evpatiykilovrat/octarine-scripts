import { AbilitiesX, PathX, Rot } from "immortal-core/Imports";
import { BaseHeroMenu } from "Menu/Base";
import { Attributes, Color, Menu } from "wrapper/Imports";
import { HERO_DATA } from "_ICore_/data";

export interface IPudgeDrawHook {
	DrawStateHook: Menu.Toggle;
	DrawStateHookColor: Menu.ColorPicker;
}

export const PudgeMenu = BaseHeroMenu({
	NpcName: "npc_dota_hero_nevermore",
	NodeAttribute: Attributes.DOTA_ATTRIBUTE_STRENGTH,
	Items: [
		"item_blink",
		"item_force_staff",
		"item_shivas_guard",
		"item_lotus_orb",
		"item_sheepstick",
		"item_minotaur_horn",
		"item_black_king_bar",
		"item_blade_mail",
		"item_ethereal_blade",
		"item_dagon_5",
		"item_orchid",
		"item_nullifier",
		"item_gungir",
		"item_rod_of_atos",
		"item_bloodthorn",
		"item_spirit_vessel",
		"item_urn_of_shadows",
	],
	Abilities: [
		"pudge_meat_hook",
		"pudge_rot",
		"pudge_dismember",
	],
	LinkenBreak: ["pudge_dismember"],
})

function OnRelease() {
	const find = AbilitiesX.All.find(x => x instanceof Rot && x.Enabled)
	if (find === undefined)
		return
	find.Owner.BaseOwner.CastToggle(find.BaseAbility)
}

PudgeMenu.ComboKey.OnRelease(() => OnRelease())
PudgeMenu.HarassKey.OnRelease(() => OnRelease())

export const ComboHookDelay = PudgeMenu.ComboSettings.AddNode("Meat Hook", PathX.DOTAAbilities("pudge_meat_hook"))
	.AddSlider("Delay (ms)", 100, 0, 500, 0, "Use ability only when enemy is moving in the same direction")

export const HarassHookDelay = PudgeMenu.HarassSettings.AddNode("Meat Hook", PathX.DOTAAbilities("pudge_meat_hook"))
	.AddSlider("Delay (ms)", 100, 0, 500, 0, "Use ability only when enemy is moving in the same direction")

const DrawStateHook = PudgeMenu.TargetMenuTree
	.DrawTargetTree.AddToggle("Draw prediciton", true, "Draw prediciton meat hook")
const DrawStateHookColor = PudgeMenu.TargetMenuTree
	.DrawTargetTree.AddColorPicker("Color prediciton", new Color(255, 0, 250))

export const PudgeDrawPrediction = {
	DrawStateHook,
	DrawStateHookColor,
}

PudgeMenu.BaseState.OnDeactivate(() => {
	HERO_DATA.ParticleManager.DestroyByKey("Hook")
	HERO_DATA.ParticleManager.DestroyByKey("FatRing")
})
PudgeDrawPrediction.DrawStateHook.OnDeactivate(() => {
	HERO_DATA.ParticleManager.DestroyByKey("Hook")
	HERO_DATA.ParticleManager.DestroyByKey("FatRing")
})

Menu.Localization.AddLocalizationUnit("russian", new Map([
	["Delay (ms)", "Задержка (мс)"],
	["Delay (ms)", "Задержка (мс)"],
	["Use ability only when enemy is moving in the same direction", "Использовать только тогда, когда враг движется в том же направлении"],
]))
