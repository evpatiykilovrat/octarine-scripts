import { AbilityX, BlackKingBar, BladeMail, BlinkDagger, Bloodthorn, Dagon, Dismember, EtherealBlade, EventsX, ForceStaff, Gleipnir, LotusOrb, MeatHook, MinotaurHorn, Nullifier, OrchidMalevolence, RodOfAtos, Rot, ScytheOfVyse, ShivasGuard, SpiritVessel, UnitX, UrnOfShadows } from "immortal-core/Imports";
import { item_aeon_disk } from "wrapper/Imports";
import { AbilityHelper, AIOBloodthorn, AIODebuff, AIODisable, AIOForceStaff, AIONuke, AIONullifier, AIOShield, HERO_DATA, ShieldBreakerMap, TargetManager } from "_ICore_/Index";
import { ModeCombo } from "../../Menu/Base";
import { BlinkDaggerPudge } from "./Abilities/BlinkDaggerPudge";
import { PudgeDismember } from "./Abilities/PudgeDismember";
import { PudgeHook } from "./Abilities/PudgeHook";
import { PudgeRot } from "./Abilities/PudgeRot";
import { IPudgeDrawHook } from "./menu";

export const data = {
	hook: undefined as Nullable<PudgeHook>,
	rot: undefined as Nullable<PudgeRot>,
	dismember: undefined as Nullable<PudgeDismember>,
	blink: undefined as Nullable<BlinkDaggerPudge>,
	force: undefined as Nullable<AIOForceStaff>,
	vessel: undefined as Nullable<AIODebuff>,
	urn: undefined as Nullable<AIODebuff>,
	atos: undefined as Nullable<AIODisable>,
	hex: undefined as Nullable<AIODisable>,
	shiva: undefined as Nullable<AIODebuff>,
	ethereal: undefined as Nullable<AIODebuff>,
	dagon: undefined as Nullable<AIONuke>,
	nullifier: undefined as Nullable<AIONullifier>,
	orchid: undefined as Nullable<AIODisable>,
	bloodthorn: undefined as Nullable<AIOBloodthorn>,
	bkb: undefined as Nullable<AIOShield>,
	horn: undefined as Nullable<AIOShield>,
	lotus: undefined as Nullable<AIOShield>,
	bladeMail: undefined as Nullable<AIOShield>,
}

export const PAbilitiesCreated = (abil: AbilityX) => {
	if (abil instanceof BlinkDagger)
		data.blink = new BlinkDaggerPudge(abil)
	if (abil instanceof MeatHook)
		data.hook = new PudgeHook(abil)
	if (abil instanceof ScytheOfVyse)
		data.hex = new AIODisable(abil)
	if (abil instanceof Rot)
		data.rot = new PudgeRot(abil)
	if (abil instanceof Dismember)
		data.dismember = new PudgeDismember(abil)
	if (abil instanceof ForceStaff)
		data.force = new AIOForceStaff(abil)
	if (abil instanceof SpiritVessel)
		data.vessel = new AIODebuff(abil)
	if (abil instanceof UrnOfShadows)
		data.urn = new AIODebuff(abil)
	if (abil instanceof OrchidMalevolence)
		data.orchid = new AIODisable(abil)
	if (abil instanceof Bloodthorn)
		data.bloodthorn = new AIOBloodthorn(abil)
	if (abil instanceof RodOfAtos || abil instanceof Gleipnir)
		data.atos = new AIODisable(abil)
	if (abil instanceof ShivasGuard)
		data.shiva = new AIODebuff(abil)
	if (abil instanceof EtherealBlade)
		data.ethereal = new AIODebuff(abil)
	if (abil instanceof Dagon)
		data.dagon = new AIONuke(abil)
	if (abil instanceof Nullifier)
		data.nullifier = new AIONullifier(abil)
	if (abil instanceof BlackKingBar)
		data.bkb = new AIOShield(abil)
	if (abil instanceof MinotaurHorn)
		data.horn = new AIOShield(abil)
	if (abil instanceof LotusOrb)
		data.lotus = new AIOShield(abil)
	if (abil instanceof BladeMail)
		data.bladeMail = new AIOShield(abil)
}

export const PudgeMode = (unit: UnitX, menu: ModeCombo) => {
	const shield = ShieldBreakerMap.get(unit)
	if (shield !== undefined)
		shield.Update(menu)

	if (HERO_DATA.ComboSleeper.Sleeping(unit.Handle) || !TargetManager.HasValidTarget)
		return false

	const target = TargetManager.Target!
	const helper = new AbilityHelper(unit, menu)

	if (helper.UseAbility(data.hex))
		return true

	if (helper.UseAbility(data.bloodthorn))
		return true

	if (helper.UseAbility(data.orchid))
		return true

	if (helper.UseAbility(data.ethereal))
		return true

	if (helper.UseAbility(data.dagon))
		return true

	if (helper.UseAbility(data.shiva))
		return true

	if (target.HasBuffByName("modifier_pudge_meat_hook")) {

		if (helper.CanBeCasted(data.bloodthorn, true, false)) {
			helper.ForceUseAbility(data.bloodthorn)
			return true
		}

		if (helper.CanBeCasted(data.orchid, true, false)) {
			helper.ForceUseAbility(data.orchid)
			return true
		}

		if (helper.CanBeCasted(data.nullifier, true, false)
			&& !target.Abilities.some(x => x.BaseAbility instanceof item_aeon_disk && x.IsReady)) {
			helper.ForceUseAbility(data.nullifier)
			return true
		}

		if (helper.CanBeCasted(data.ethereal, true, false)) {
			helper.ForceUseAbility(data.ethereal)
			return true
		}

		if (helper.UseAbility(data.bkb))
			return true

		if (helper.UseAbility(data.horn))
			return true

		if (helper.UseAbility(data.lotus))
			return true

		if (helper.UseAbility(data.dagon))
			return true

		if (helper.UseAbility(data.vessel))
			return true

		if (helper.UseAbility(data.urn))
			return true
	}

	if (helper.CanBeCasted(data.dismember)) {
		if (helper.CanBeCasted(data.orchid, true, false)) {
			helper.ForceUseAbility(data.orchid)
			return true
		}

		if (helper.CanBeCasted(data.bloodthorn, true, false)) {
			helper.ForceUseAbility(data.bloodthorn)
			return true
		}

		if (helper.CanBeCasted(data.nullifier, true, false)
			&& !target.Abilities.some(x => x.BaseAbility instanceof item_aeon_disk && x.IsReady)) {
			helper.ForceUseAbility(data.nullifier)  // TODO
			return true
		}

		if (helper.UseAbility(data.bkb))
			return true

		if (helper.UseAbility(data.horn))
			return true

		if (helper.UseAbility(data.lotus))
			return true

		if (helper.CanBeCasted(data.ethereal, true, false)) {
			helper.ForceUseAbility(data.ethereal)
			return true
		}

		if (helper.UseAbility(data.dagon)) {
			return true;
		}

		if (helper.UseAbility(data.bladeMail))
			return true

		if (helper.UseAbility(data.vessel))
			return true

		if (helper.UseAbility(data.urn))
			return true

		helper.ForceUseAbility(data.dismember, true)
		return true
	}

	if (helper.UseAbilityIfCondition(data.blink, data.dismember))
		return true

	if (helper.UseAbility(data.blink, 800, 25))
		return true

	if (helper.UseAbility(data.atos))
		return true

	if (helper.CanBeCasted(data.force) && helper.CanBeCasted(data.hook)) {
		if (data.force?.UseAbilityOnTarget())
			return true
	}

	if (helper.UseAbility(data.force, 400, 800))
		return true

	if (helper.UseAbility(data.hook))
		return true

	if (helper.CanBeCasted(data.rot, false, false)) {
		if (data.rot?.AutoToggle())
			return true
	}

	return false
}

export const DrawHookPosition = (unit: UnitX, menu: IPudgeDrawHook) => {
	if (!menu.DrawStateHook.value)
		return
	const target = TargetManager.Target!
	if (!TargetManager.HasValidTarget || unit.Distance(target) > TargetManager.TargetDistance) {
		HERO_DATA.ParticleManager.DestroyByKey("Hook")
		HERO_DATA.ParticleManager.DestroyByKey("FatRing")
		return
	}

	const predicition = data.hook?.GetPrediction
	if (predicition === undefined)
		return

	const pos = unit.Position.Extend(predicition.CastPosition, unit.Distance(target, true))
	HERO_DATA.ParticleManager.DrawLine("Hook", unit.BaseOwner, pos.Extend(target.Position, 80), {
		Position: target.Position.Extend(pos, 80),
		Alpha: target.Distance(pos) > 80 ? 255 : 0,
		Width: 50,
		Mode2D: 0,
		Color: menu.DrawStateHookColor.selected_color,
	})
	HERO_DATA.ParticleManager.DrawFatRing("FatRing", unit.BaseOwner, {
		Position: pos,
		Color: menu.DrawStateHookColor.selected_color,
	})
}

EventsX.on("GameEnded", () => {
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			// @ts-ignore
			data[key] = undefined
		}
	}
})
